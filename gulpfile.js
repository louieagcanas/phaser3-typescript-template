var gulp = require( 'gulp' );
var gulpWatch = require( 'gulp-watch' );
var browserify = require( 'browserify' );
var tsify = require( 'tsify' );
var source = require( 'vinyl-source-stream' );
var buffer = require( 'vinyl-buffer' );
var uglify = require( 'gulp-uglify' );
var sourcemaps = require( 'gulp-sourcemaps' );
var browserSync = require( 'browser-sync' ).create();
var del = require( 'del' );


const paths = {
    source: './src/',
    public: './public/',
    assets: './assets/',
    static: './static/'
};

function cleanBuild(){
    return del( [paths.public] );
}

function copyStatic(){
    return gulp
        .src( paths.static + '/**/*' )
        .pipe( gulp.dest( paths.public ) );
}

function copyAssets(){
    return gulp
        .src( paths.assets + '/**/*' )
        .pipe( gulp.dest( paths.public + 'assets' ) );
}

function copyPhaser(){
    return gulp
        .src( 'node_modules/phaser/dist/phaser.min.js/' )
        .pipe( gulp.dest( paths.public + 'src' ) );
}

function watch(){
    let files = [
        paths.static + '/**/*',
        paths.assets + '/**/*',
        paths.source + '/**/*.ts'
    ];

    gulpWatch( files, () => {
        gulp.start( 'reload' );
    });
}

function build(){
    return browserify({
        basedir: '.',
        debug: true,
        entries: ['src/game.ts'],
        cache: {},
        packageCache: {}
    })
    .plugin( tsify )
    .transform( 'babelify', {
        presets: ['@babel/preset-env'],
        extensions: ['.ts']
    })
    .exclude( 'phaser' )
    .bundle()
    .pipe( source( 'game.js' ) )
    .pipe( buffer() )
    .pipe( sourcemaps.init( { loadMaps: true } ) )
    .pipe( uglify() )
    .pipe( sourcemaps.write( './' ) )
    .pipe( gulp.dest( paths.public + '/src' ) );
}

function serve(){
    browserSync.init({
        server: paths.public,
        open: true
    });
}

function reload(){
    browserSync.reload();
}

gulp.task( 'clean', cleanBuild );
gulp.task( 'copy-static', ['clean'], copyStatic );
gulp.task( 'copy-assets', ['copy-static'], copyAssets );
gulp.task( 'copy-phaser', ['copy-assets'], copyPhaser );
gulp.task( 'watch', ['copy-phaser'], watch );
gulp.task( 'reload', ['build'], reload );
gulp.task( 'build', ['watch'], build ) ;
gulp.task( 'serve', ['build'], serve );
